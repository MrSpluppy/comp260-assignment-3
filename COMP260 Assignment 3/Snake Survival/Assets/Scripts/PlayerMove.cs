﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {


    public float moveDist = 1f;
    private Vector2 moveDirection;
    public float moveInterval = 0.5f;
    private float moveIntervalTime = 0f;
    public GameObject player;
    ShellMenu shellMenu;
    public MapNavigation map = null;

    Timer clock;

    //public CoinSpawner coin;

    public AudioClip[] coinGet = new AudioClip[4];
    private AudioSource audioSource;

    // Use this for initialization
    void Start () {
        //default player starts by moving upwards
        moveDirection = new Vector2(0, 1);
        map = (MapNavigation)FindObjectOfType(typeof(MapNavigation));
        audioSource = GetComponent<AudioSource>();
        shellMenu = (ShellMenu)FindObjectOfType(typeof(ShellMenu));
        clock = (Timer)FindObjectOfType(typeof(Timer));
    }

    // Update is called once per frame
    void Update()
    {
        //set player's previous location as being 'empty'
        //map.gameboard[(int)(this.gameObject.transform.position.x * 2), (int)(this.gameObject.transform.position.y * -2)].setIsOccupied(false);

        //tracks which direction the player will be moving
        if (Input.GetAxis("Vertical") > 0) //w
        {
            moveDirection = new Vector2(0, 1);
            this.gameObject.transform.rotation = Quaternion.AngleAxis(0f, Vector3.forward);
        }
        else if (Input.GetAxis("Vertical") < 0) //s
        {
            moveDirection = new Vector2(0, -1);
            this.gameObject.transform.rotation = Quaternion.AngleAxis(180f, Vector3.forward);
        }
        if (Input.GetAxis("Horizontal") < 0) //a
        {
            moveDirection = new Vector2(-1, 0);
            this.gameObject.transform.rotation = Quaternion.AngleAxis(90f, Vector3.forward);
        }
        else if (Input.GetAxis("Horizontal") > 0) //d 
        {
            moveDirection = new Vector2(1, 0);
            this.gameObject.transform.rotation = Quaternion.AngleAxis(270f, Vector3.forward);
        }

            //moves the player in intervals
            moveIntervalTime += Time.deltaTime;
        if (moveIntervalTime >= moveInterval)
        {
            //Adds player direction and their current position to move to new location
            Vector2 currentPosition = transform.position;
            Vector2 newPos = currentPosition + (moveDirection / moveDist);
            //print(newPos);
            transform.position = newPos;
            //map.gameboard[(int)newPos.x, (int)newPos.y].setIsOccupied(true);
            moveIntervalTime = 0f;

            //this code returns the player's location in relation to the gameboard
            //print(map.gameboard[(int)(this.gameObject.transform.position.x * 2), (int)(this.gameObject.transform.position.y * -2)].getLocation());

            //set player's location as being 'occupied'
            map.gameboard[(int)(this.gameObject.transform.position.x * 2), (int)(this.gameObject.transform.position.y * -2)].setIsOccupied(true);
            print(map.gameboard[(int)(this.gameObject.transform.position.x * 2), (int)(this.gameObject.transform.position.y * -2)].getIsOccupied());
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Contains("Coin") == true)
        {
            audioSource.PlayOneShot(coinGet[Random.Range(0, coinGet.Length)]);
        }
        else
        {
            shellMenu.gameoverPanel.SetActive(true);
            shellMenu.gamePanel.SetActive(false);
            print("shiet");
        }
    }

    public void resetToLoadState(GameObject player)
    {
        player.transform.position = map.gameboard[(int)((map.width -1) / 2), (int)((map.height -1) / 2)].getLocation();
        clock.resetToLoadState();
        print(map.gameboard[map.width / 2, map.height / 2].getLocation());
    }
}

