﻿using UnityEngine;
using System.Collections;

public class SnakeMove : MonoBehaviour {

    public MapNavigation map = null;
    public GameObject[] snake1Parts;
    public GameObject[] snake2Parts;
    public GameObject[] snake3Parts;
    public GameObject[] snake4Parts;

    public float[] snake1Rotation = new float[4];
    public float[] snake2Rotation = new float[4];
    public float[] snake3Rotation = new float[4];
    public float[] snake4Rotation = new float[4];

    SnakeObject snakeObject;

    public float moveDist = 1f;
    public float moveInterval = 0.5f;
    public float moveIntervalTime = 0f;

    // Use this for initialization
    void Start () {
        // instantiate objects
        map = (MapNavigation)FindObjectOfType(typeof(MapNavigation));
        snakeObject = (SnakeObject)FindObjectOfType(typeof(SnakeObject));

        //populate snake data arrays 
        for (int i = 0; i < snake1Rotation.Length; i++)
        {
            snake1Rotation[i] = 270f;
            snake3Rotation[i] = 270f;
        }
        for (int i = 0; i < snake1Rotation.Length; i++)
        {
            snake2Rotation[i] = 90f;
            snake4Rotation[i] = 90f;
        }

        Snake snake1 = new Snake(snake1Rotation, snake1Parts);
        Snake snake2 = new Snake(snake2Rotation, snake2Parts);
        Snake snake3 = new Snake(snake3Rotation, snake3Parts);
        Snake snake4 = new Snake(snake4Rotation, snake4Parts);
    }
	
	// Update is called once per frame
	void Update () {
        // 0=up, 90=left, 180=down, 270=right



        //set snake's previous location as being 'empty'
        for (int i = 0; i < snake1Parts.Length; i++) {
            map.gameboard[(int)(this.gameObject.transform.position.x * 2), (int)(this.gameObject.transform.position.y * -2)].setIsOccupied(false);
        }
        

        //moves the snake in intervals
        moveIntervalTime += Time.deltaTime;
        if (moveIntervalTime >= moveInterval)
        {
            /*Adds snake direction and their current position to move to new location
            Vector2 currentPosition = transform.position;
            Vector2 newPos = currentPosition + (moveDirection / moveDist);
            //print(newPos);
            transform.position = newPos;
            //map.gameboard[(int)newPos.x, (int)newPos.y].setIsOccupied(true);
            lifetime = 0f;

            //this code returns the player's location in relation to the gameboard
            //print(map.gameboard[(int)(this.gameObject.transform.position.x * 2), (int)(this.gameObject.transform.position.y * -2)].getLocation());

            //set player's location as being 'occupied'
            map.gameboard[(int)(this.gameObject.transform.position.x * 2), (int)(this.gameObject.transform.position.y * -2)].setIsOccupied(true);
            */
        }
    }



    public class Snake
    {
        private Vector2[] location;
        private float[] rotation;
        private GameObject[] snakeBodyParts;

        public Snake(float[] rot, GameObject[] body)
        {
            rotation = rot;
            snakeBodyParts = body;
            for (int i = 0; i < snakeBodyParts.Length; i++)
            {
                location[i] = this.getLocation(i);
            }
            for (int i = 0; i < rotation.Length; i++)
            {
                rotation[i] = this.getRotation(i);
            }
        }

        public void setRotation(float numDegrees, int snakePart)
        {
            snakeBodyParts[snakePart].transform.rotation = Quaternion.AngleAxis(numDegrees, Vector3.forward);
        }

        public float getRotation(int m)
        {
            return snakeBodyParts[m].transform.rotation.z;
        }

        public void swapSnakeBodyPart(int target1, int target2)
        {
            GameObject temp = snakeBodyParts[target1];
            snakeBodyParts[target1] = snakeBodyParts[target2];
            snakeBodyParts[target2] = temp;
        }

        public Vector2 getLocation(int whichBodyPart)
        {
            return snakeBodyParts[whichBodyPart].transform.position;
        }

        public void setLocation(int whichBodyPart, Vector2 newLocation)
        {
            snakeBodyParts[whichBodyPart].transform.position = newLocation;
        }

    }
}
