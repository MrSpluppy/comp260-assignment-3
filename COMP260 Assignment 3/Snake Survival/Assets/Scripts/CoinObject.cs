﻿using UnityEngine;
using System.Collections;
using System;

public class CoinObject : MonoBehaviour {

    public CoinSpawner spawner;

    public AudioClip[] coinGet;
    public AudioClip coinLose;
    //private AudioClip main;
    public AudioSource clip;


    // Use this for initialization
    void Start () {
        clip = GetComponent<AudioSource>();
        spawner = (CoinSpawner)FindObjectOfType(typeof(CoinSpawner));
    }

    // Update is called once per frame
    void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            spawner.addPoint();
            clip.PlayOneShot(coinGet[UnityEngine.Random.Range(0, coinGet.Length)]);
        }
        else
        {
            clip.PlayOneShot(coinLose);
        }
        Destroy(gameObject);
    }
}
