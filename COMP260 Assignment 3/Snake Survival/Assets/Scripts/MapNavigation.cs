﻿using UnityEngine;
using System.Collections;

public class MapNavigation : MonoBehaviour {

    public int width;
    public int height;
    public gridSpace[,] gameboard;


    // Use this for initialization
    void Start()
    {
        gameboard = new gridSpace[width,height];


        for (float y = 0; y < height; y++)
        {
            for (float x = 0; x < width; x++)
            {
                gameboard[(int) x ,(int) y] = new gridSpace(false, new Vector2(x/2, -y/2));
                //print(gameboard[(int)x, (int)y].getLocation());
            }
        }
    }

    public class gridSpace
    {
        private bool isOccupied;
        private Vector2 Location;

        public gridSpace(bool Occ, Vector2 Loc)
        {
            isOccupied = Occ;
            Location = Loc;
        }

        public bool getIsOccupied()
        {
            return isOccupied;
        }
        public void setIsOccupied(bool b)
        {
            isOccupied = b;
        }

        public Vector2 getLocation()
        {
            return Location;
        }
    }
}
