﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CoinSpawner : MonoBehaviour
{

    public Text score;
    private int points;
    private int coinNumber = 1;
    public int coinMax = 1;
    private int coinCurrent = 0;

    CoinObject coinPrefab;
    MapNavigation map;

    // Use this for initialization
    void Start()
    {
        //set components
        coinPrefab = (CoinObject)FindObjectOfType(typeof(CoinObject));
        map = (MapNavigation)FindObjectOfType(typeof(MapNavigation));
        SpawnCoin();
        coinCurrent++;
    }

    void Update()
    {
        if (points == 5)
            coinMax = 2;
        if (points == 15)
            coinMax = 3;
        if (points == 25)
            coinMax = 4;
        while (coinCurrent < coinMax)
        {
            SpawnCoin();
            coinCurrent++;
        }
        //SpawnCoin();
    }

    public void SpawnCoin()
    {
        Vector2 temp = new Vector2(-1, -1);
        // instantiate a coin
        CoinObject coin = (CoinObject) Instantiate(coinPrefab, temp, Quaternion.identity);
        // attach to this object in the hierarchy
        coin.transform.parent = transform;
        // give coin a tag
        coin.transform.tag = "Coin";
        // give the coin a name and number
        coin.gameObject.name = ("Coin " + coinNumber);
        coinNumber++;

        bool foundSpace = false;
        while (!foundSpace)
        {
            temp.x = Random.Range(0, map.width);
            temp.y = Random.Range(0, map.height);
            //print("coin location: " + temp);
            if (map.gameboard[(int) temp.x, (int) temp.y].getIsOccupied())
                return;
            else
            {
                // Once it finds an empty spot, it sets that location to 'occupied' and moves the coin object there.
                map.gameboard[(int)temp.x, (int)temp.y].setIsOccupied(true);
                coin.transform.position = new Vector2(temp.x, -temp.y)/2;
                foundSpace = true;
            }

        }

        if(temp.x == -1 || temp.y == -1)
        {
            print("Failed to spawn coin");
        }
    }

    public void addPoint()
    {
        // CoinSpawner coin = (CoinSpawner)FindObjectOfType(typeof(CoinSpawner));
        // MapNavigation map = (MapNavigation)FindObjectOfType(typeof(MapNavigation));

        points++;
        if (points < 10)
            this.score.text = "SCORE: 000" + points;
        else
            this.score.text = "SCORE: 00" + points;
        coinCurrent--;
    }

    public void resetToLoadState()
    {
        points = 0;
        coinNumber = 1;
        coinMax = 1;
        coinCurrent = 0;
        this.score.text = "SCORE: 000" + points;
        DestroyGameObjectsWithTag("Coin");

    }

    public void DestroyGameObjectsWithTag(string tag)
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(tag);
        foreach (GameObject target in gameObjects)
        {
            GameObject.Destroy(target);
        }
    }
}
