﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {




    public Text timer;
    private float timeSeconds;
    private int timeMinutes;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (timeSeconds >= 60)
        {
            timeSeconds = 0;
            timeMinutes++;
        }
        if (timeSeconds < 10)
            timer.text = "TIME " + timeMinutes + ":0" + (int)timeSeconds;
        else
            timer.text = "TIME " + timeMinutes + ":" + (int)timeSeconds;

        timeSeconds += Time.deltaTime;
    }

    public void resetToLoadState() {
     timeSeconds = 0;
     timeMinutes = 0;
    }

}
